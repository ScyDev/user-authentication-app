const mongoose = require("mongoose");
var bcrypt = require("bcrypt-nodejs");



    const userSchema = mongoose.Schema({

        local            : {
            userJwtSalt  : String,
            email        : String,
            password     : String,
        },
        facebook         : {
            id           : String,
            token        : String,
            name         : String,
            email        : String
        },
        twitter          : {
            id           : String,
            token        : String,
            displayName  : String,
            username     : String
        },
        google           : {
            id           : String,
            token        : String,
            email        : String,
            name         : String
        }
    
    
    
});



///////////////////////////
//////methods//////////////
///////////////////////////



// generating a hasch with bcrypt-node.js

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

};


// checking if PW is vallid 

userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);

};



// creating the model for users expose it to our app
const User = mongoose.model("User", userSchema)
module.exports = User;
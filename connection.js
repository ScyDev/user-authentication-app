
const mongoose = require("mongoose");
const User = require("./models/user");
const connection = "mongodb://critically-entangled-authApp-db:27017/mongo";


const connectDb = () => {
 return mongoose.connect(connection);
};

module.exports = connectDb;